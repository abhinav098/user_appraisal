Rails.application.routes.draw do
	
  resources :user_appraisals
  root 'user_appraisals#index'
  get '/my_appraisals'  => 'user_appraisals#my_appraisals'
  get '/user_appraisal/:user_id'  => 'user_appraisals#user_appraisal'
  #devise routes
  devise_for :users, path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    sign_up: 'register'
  }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
