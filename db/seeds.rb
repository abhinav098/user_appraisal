# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
users = [
  {email: 'sharad@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'vivek@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'twinkle@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'abhinav@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'abhimanyu@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'akash@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'shruti@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'harshita@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'},
  {email: 'himanshu@prepsmarter.com', password: 'hello123', password_confirmation: 'hello123'}
]
users.each do |usr|
  User.create(usr)
end

categories = ['Humanity', 'Integrity', "Discipline", 'Working Together']
categories.each do |name|
  Category.create(name: name)
end
