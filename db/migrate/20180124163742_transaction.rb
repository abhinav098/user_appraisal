class Transaction < ActiveRecord::Migration[5.1]
  def change
	  create_table :transactions do |t|
	    t.references :category, foreign_key: true
	    t.references :appraisee, index: true
	    t.references :appraiser, index: true
	  end  
  end
  # add_foreign_key :transactions, :users, column: :appraisee_id, primary_key: :id
  # add_foreign_key :transactions, :users, column: :appraiser_id, primary_key: :id
end
