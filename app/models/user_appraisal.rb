class UserAppraisal < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :transactions

  validates :comment, presence: true
  validates :points, presence: true

end
