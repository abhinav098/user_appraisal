class Transaction < ApplicationRecord
  belongs_to :appraisee, class_name: 'User'
  belongs_to :appraiser, class_name: 'User' #current_user
  belongs_to :category
end