class UserAppraisalsController < ApplicationController
  before_action :set_appraisal, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!

  def index
    @users = User.where.not(id: current_user)
    @categories = Category.all
  end

  def my_appraisals
    @user_appraisals = current_user.user_appraisals
  end

  def new
    @user_appraisal = UserAppraisal.new
  end

  def user_appraisal
    return redirect_to user_appraisals_url, alert: "You are not authorised to perform this action" unless current_user.manager?
    @user = User.find(params[:user_id])
    @user_appraisals = @user.user_appraisals
  end

  def create
    @user_appraisal = UserAppraisal.new(appraisal_params)
    if current_user.points >= appraisal_params[:points].to_i && @user_appraisal.save
      complete_transaction
      redirect_to user_appraisals_url
    else
      flash[:notice] = "Points exhausted, should be less than #{current_user.points} !"
      redirect_to user_appraisals_url
    end
  end

  def update
    if @user_appraisal.update(appraisal_params)
			redirect_to @appraisal, notice: 'appraisal was successfully updated.'
    end
  end

  def destroy
    @user_appraisal.destroy
    redirect_to user_appraisals_url, notice: 'Appraisal was successfully destroyed.'
  end

	private
  def set_appraisal
    @user_appraisal = Appraisal.find(params[:id])
  end

  def complete_transaction
    Transaction.create(appraisee: @user_appraisal.user, appraiser: current_user, category: @user_appraisal.category)
    current_user.update points: (current_user.points - appraisal_params[:points].to_i)
  end

  def appraisal_params
    params.require(:user_appraisal).permit(:user_id, :category_id, :comment, :points)
  end
end
